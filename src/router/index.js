import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Persons from '@/components/admin/Persons'
import PersonDetail from '@/components/admin/PersonDetail'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/admin/persons',
      name: 'Persons',
      component: Persons
    },
    {
      path: '/admin/person/:id',
      name: 'PersonDetail',
      component: PersonDetail
    }
  ]
})
